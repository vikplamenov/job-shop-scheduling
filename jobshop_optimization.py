# -*- coding: utf-8 -*-
"""
Created on Sat Nov  6 21:01:56 2021

@author: Viktor
"""

from ortools.sat.python import cp_model
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import collections



# Define the scheduler optimizer
def JobScheduler(jobs_list, teams = 3, runtime_constraint_secs = 60):
    """jobshop problem."""
    
    # list of jobs/orders
    jobs_data = jobs_list
    
    staff_groups_count = teams
    all_staff_teams    = range(staff_groups_count)
    
    # Computes horizon dynamically as the sum of all durations.
    horizon = sum(task[1] for job in jobs_data for task in job)

    # Create the model.
    model = cp_model.CpModel()

    # Named tuple to store information about created variables.
    task_type = collections.namedtuple('task_type', 'start end interval')
    
    # Named tuple to manipulate solution information.
    assigned_task_type = collections.namedtuple('assigned_task_type', 'start job index duration')

    # Creates job intervals and add to the corresponding machine lists.
    all_tasks = {}
    machine_to_intervals = collections.defaultdict(list)

    for job_id, job in enumerate(jobs_data):
        for task_id, task in enumerate(job):
            machine   = task[0]
            duration  = task[1]
            suffix    = '_%i_%i' % (job_id, task_id)
            start_var = model.NewIntVar(0, horizon, 'start' + suffix)
            end_var   = model.NewIntVar(0, horizon, 'end' + suffix)
            interval_var = model.NewIntervalVar(start_var, duration, end_var,
                                                'interval' + suffix)
            all_tasks[job_id, task_id] = task_type(start = start_var,
                                                   end   = end_var,
                                                   interval = interval_var)
            machine_to_intervals[machine].append(interval_var)

    # Create and add disjunctive constraints.
    for machine in all_staff_teams:
        model.AddNoOverlap(machine_to_intervals[machine])

        
    # Precedences inside a job.
    for job_id, job in enumerate(jobs_data):
        for task_id in range(len(job) - 1):
            model.Add(all_tasks[job_id, task_id +1].start >= all_tasks[job_id, task_id].end)

            
    # Makespan objective.
    obj_var = model.NewIntVar(0, horizon, 'makespan')
    model.AddMaxEquality(obj_var, [all_tasks[job_id, len(job) - 1].end
        for job_id, job in enumerate(jobs_data)
    ])
    
    model.Minimize(obj_var)

    # Creates the solver and solve.
    solver = cp_model.CpSolver()      
    solver.parameters.max_time_in_seconds = runtime_constraint_secs
    status = solver.Solve(model)

    if status == cp_model.OPTIMAL or status == cp_model.FEASIBLE:
        print('Solution:')
        # Create one list of assigned tasks per machine.
        assigned_jobs = collections.defaultdict(list)
        for job_id, job in enumerate(jobs_data):
            for task_id, task in enumerate(job):
                machine = task[0]
                assigned_jobs[machine].append(
                    assigned_task_type(start=solver.Value(
                        all_tasks[job_id, task_id].start),
                                       job = job_id,
                                       index = task_id,
                                       duration = task[1]))

        # Create per machine output lines.
        output = ''
        res = list()
        for machine in all_staff_teams:
            
            # Sort by starting time.
            assigned_jobs[machine].sort()
            sol_line_tasks = 'Machine ' + str(machine) + ': '
            sol_line = '           '
            
            for assigned_task in assigned_jobs[machine]:
                name = 'job_%i_task_%i' % (assigned_task.job,
                                           assigned_task.index)
                # Add spaces to output to align columns.
                sol_line_tasks += '%-15s' % name               

                start = assigned_task.start
                duration = assigned_task.duration
                sol_tmp = '[%i,%i]' % (start, start + duration)
                res.append(('team_' + str(machine), 
                            'order_' + str(assigned_task.job) + "_operation_" + str(assigned_task.index),
                           start, start+duration))
                
                # Add spaces to output to align columns.
                sol_line += '%-15s' % sol_tmp
           
            output += sol_line_tasks
            output += sol_line
        print(f'Optimal Schedule Length: {solver.ObjectiveValue()}')
        return list((output, solver.ObjectiveValue(), res))
    else:
        print('No solution found.')
        
        
        
# Readr the R generated file and prepare it for the optimization
orders = pd.read_csv("orders_input.csv")
orders['op_resource_group_num'] = orders['op_resource_group_num'] - 1
#orders['total_op_time_sec'] =  pd.to_numeric(np.round(orders.total_op_time_sec), downcast = 'integer')
orders_df = orders.groupby(by="order_code")[["op_resource_group_num", 'total_op_time_sec']].agg(tuple)

params = pd.read_csv('scheduler_params.csv')

# Prepare the data to pass to the optimizer
tmp_list = list()
end_list = list()
n_orders = params.n_orders.values[0]

for i in range(n_orders):
    l = len(orders_df.op_resource_group_num.values[i])       
    tmp_list.append(list((orders_df.op_resource_group_num.values[i][:l], 
                          orders_df.total_op_time_sec.values[i][:l]
                            )
                          )
                     )
    
    
for i in range(n_orders):
    end_list.append(list(map(tuple, zip(*tmp_list[i]))))       


# Run the optimizer
n_teams = len(orders.op_resource_group_num.unique())
solution = JobScheduler(jobs_list = end_list, teams = n_teams, runtime_constraint_secs = 20)



# extract the proposed schedule
schedule_df = pd.DataFrame(solution[2])
schedule_df.columns   = ['team', 'job_task', "start_time", 'end_time']
schedule_df['spread'] = np.abs(schedule_df.start_time - schedule_df.end_time)
schedule_df['order'] = schedule_df['job_task'].astype(str).str[:8]

